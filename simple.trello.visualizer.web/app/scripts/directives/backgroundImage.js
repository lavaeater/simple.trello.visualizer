'use strict';

angular.module('simpletrellovisualizerwebApp')
  .directive('backImg', function(){
    return function(scope, element, attrs){
        attrs.$observe('backImg', function(value) {
            element.css({
                'background-image': 'url(' + value +')',
                'background-size' : 'cover',
                'background-repeat' : 'no-repeat'
            });
        });
    };
});

//background-image: url(http://beamonpeople.se/themes/beamon/images/bubbles_start.png); background-repeat: no-repeat