'use strict';

angular.module('simpletrellovisualizerwebApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngLocale',
  'LocalStorageModule',
  'yaru22.md',
  'ui.utils'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/trello', {
        templateUrl: 'views/trello.html',
        controller: 'TrelloCtrl'
      })
      .when('/boards', {
        templateUrl: 'views/boards.html',
        controller: 'BoardsCtrl'
      })
      .when('/board/:id', {
        templateUrl: 'views/board.html',
        controller: 'BoardCtrl'
      })
      .when('/list/:id', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
      })
      .when('/list/:id/:card', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
      })
      .when('/card', {
        templateUrl: 'views/card.html',
        controller: 'CardCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
