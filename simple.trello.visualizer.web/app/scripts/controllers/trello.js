'use strict';

angular.module('simpletrellovisualizerwebApp')
  .controller('TrelloCtrl', function ($scope, $location, UserSettingsService) {

    $scope.appKey = UserSettingsService.appKey();
    $scope.readOnlyToken = UserSettingsService.readOnlyToken();
    $scope.userSettings = UserSettingsService.userSettings();


    $scope.setTrelloStuff = function() {
      UserSettingsService.appKey($scope.appKey);
      UserSettingsService.readOnlyToken($scope.readonlyToken);

      UserSettingsService.userSettings($scope.userSettings);

      $location.url('boards');
    };


  });
