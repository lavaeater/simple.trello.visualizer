'use strict';

angular.module('simpletrellovisualizerwebApp')
  .controller('BoardsCtrl', function ($scope, Trelloservice) {
    Trelloservice.getBoards().then(function(data) {
    	$scope.boards = data;
    });
  });
