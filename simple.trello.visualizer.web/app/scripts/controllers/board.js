'use strict';

angular.module('simpletrellovisualizerwebApp')
  .controller('BoardCtrl', function ($scope, $route, $routeParams, Trelloservice) {

  	Trelloservice.getLists($routeParams.id).then(function(data) {
  		$scope.lists = data.lists;
  	}) ;

  });
