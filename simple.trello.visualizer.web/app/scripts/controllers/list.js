'use strict';

angular.module('simpletrellovisualizerwebApp')
  .controller('ListCtrl', function ($scope, $routeParams,Trelloservice, UserSettingsService) {

  	$scope.currentListId = $routeParams.id;
  	$scope.cardIndex = 0;
  	$scope.cards = [];
    $scope.backgroundImgUrl = function() {
      return UserSettingsService.userSettings().backgroundImageUrl;
    };

  	Trelloservice.getCards($routeParams.id).then(function(data) {
  		$scope.cards = data.cards;

  		$scope.setCurrentCard();
  	});

  	$scope.checkBounds = function() {
		if($scope.cardIndex >= $scope.cards.length) {
	  		$scope.cardIndex = 0;
	  	}

		if($scope.cardIndex < 0) {
	  		$scope.cardIndex = $scope.cards.length - 1;
	  	}
  	};

  	$scope.setCurrentCard = function() {
  		$scope.checkBounds();
  		$scope.currentCard = $scope.cards[$scope.cardIndex];

  	};

  	$scope.nextCard = function() {
  		$scope.cardIndex++;
  		$scope.setCurrentCard();
  	};

  	$scope.previousCard = function() {
  		$scope.cardIndex--
  		$scope.setCurrentCard();
  	};

    $scope.keypressCallback = function($event) {
      if($event.keyCode === 39) {
        $event.preventDefault();
        $scope.nextCard();
      }
      if($event.keyCode === 37) {
        $event.preventDefault();
        $scope.previousCard();
      }

    };

  });
