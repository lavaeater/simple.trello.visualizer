'use strict';

angular.module('simpletrellovisualizerwebApp')
  .service('Trelloservice', function Trelloservice($http, $q, UserSettingsService) {
    // AngularJS will instantiate a singleton by calling 'new' on this function
    var self = this;
    var trelloBaseUrl = 'https://trello.com/1';

    self.getBoards = function() {
    	var command = '/members/my/boards?' + UserSettingsService.trelloKey();
    	var deferred = $q.defer();
			$http({ method: 'GET', url: trelloBaseUrl + command }).
				success(function (data, status, headers, config) {	
					deferred.resolve(data);
				}).
				error(function(data, status, headers, config) {
					deferred.reject(status);
				});
			
			return deferred.promise;
    };

    self.getLists = function(boardId) {
    var command = '/boards/'+boardId+'?lists=open&' + UserSettingsService.trelloKey();
        var deferred = $q.defer();
            $http({ method: 'GET', url: trelloBaseUrl + command }).
                success(function (data, status, headers, config) {  
                    deferred.resolve(data);
                }).
                error(function(data, status, headers, config) {
                    deferred.reject(status);
                });
            
            return deferred.promise;
    };

    self.getCards = function(listId) {
        var command = '/lists/' + listId + '?fields=name&cards=open&card_fields=name,desc&' + UserSettingsService.trelloKey();
        var deferred = $q.defer();
            $http({ method: 'GET', url: trelloBaseUrl + command }).
                success(function (data, status, headers, config) {  
                    deferred.resolve(data);
                }).
                error(function(data, status, headers, config) {
                    deferred.reject(status);
                });
            
            return deferred.promise;  
    };

  });
