'use strict';

angular.module('simpletrellovisualizerwebApp')
.service('UserSettingsService', function UserSettingsService(localStorageService) {
    // AngularJS will instantiate a singleton by calling 'new' on this function
    var self = this;
    var trelloBaseUrl = 'https://trello.com/1';

    self.userSettings = function(settings) {
        if(typeof(settings) !== 'undefined') {
            var serialized = angular.toJson(settings);
            self.setAndGetUserStoreValue('user_settings', serialized)
        }
        var returnVal = self.setAndGetUserStoreValue('user_settings');
        return angular.fromJson(returnVal);
    }

    self.appKey = function(appKey) {
        //if we pass a value, we update the store
        return self.setAndGetUserStoreValue('trello_app_key', appKey);
    };

    self.readOnlyToken = function(readOnlyToken) {
        return self.setAndGetUserStoreValue('trello_read_only_token', readOnlyToken);
    };

    self.trelloKey = function() {
      return 'key='+ self.appKey() + '&token=' + self.readOnlyToken();  
  };

  self.setAndGetUserStoreValue = function(storeName, value) {

    if(typeof(value) != 'undefined') {
        localStorageService.add(storeName, value);
    }
    return localStorageService.get(storeName);  
};
});
